public class Calculator{
//by JDX
/*

	||	||||\	\\  //
	||	|   \\	 \\//
	||	|   //	 //\\
	//	||||/	//  \\

*/
  public static void main(String [] args){
	if (args.length == 3){
	  switch (args[1]){
		 case "+": System.out.println(Double.valueOf(args[0]) + Double.valueOf(args[2]));
		   break;
		 case "-": System.out.println(Double.valueOf(args[0]) - Double.valueOf(args[2]));
		   break;
		 case "m": System.out.println(Double.valueOf(args[0]) * Double.valueOf(args[2]));
		   break;
		 case "/": System.out.println(Double.valueOf(args[0]) / Double.valueOf(args[2]));
		   break;
         default: System.out.println("Thats not a valid operator.");
	  }
    } else {
        System.out.println(args.length);
		System.out.println("Usage: java Calculator firstNumber Operator secondNumber");
	  }

  }
}